require('dotenv').config()
const res = require('express/lib/response');
const request = require('supertest')
const app = require('../../app')
const {Car} = require('../../app/models')
jest.setTimeout(100000)


describe('get test (/v1/cars/:id)', () => {
    let data;

    beforeEach(async () => {
        data = await Car.create({
            name:"avanza",
            price:1000,
            size:"SMALL",
            image:"avanza.jpg",
            isCurrentlyRented:false,
            createdAt:new Date(),
            updatedAt : new Date()
        })
    }) 
    
    it('jika status respon code 200', () => {
        return request(app)
            .get("/v1/cars/"+ data.id)
            .then(res => {
                expect(res.status).toBe(200)
                expect(res.body).toEqual({
                    id:data.id,
                    name:data.name,
                    price:data.price,
                    size:data.size,
                    image:data.image,
                    isCurrentlyRented:data.isCurrentlyRented,
                    createdAt: data.createdAt.toJSON(),
                    updatedAt: data.updatedAt.toJSON()
                })
            })
    });
    
});