require('dotenv').config()
const res = require('express/lib/response');
const request = require('supertest')
const app = require('../../app')
jest.setTimeout(100000)

describe('register test (/v1/auth/register)', () => {
    it('jika status respon code 201', () => {
        return request(app)
            .post('/v1/auth/register')
            .send({
                name:"sri",
                email:"sri@binar.co.id",
                password:"123456"  
            })
            .then(res => {
                expect(res.status).toBe(201)
            })
    });
    
});
