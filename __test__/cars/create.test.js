require('dotenv').config()
const request = require('supertest')
const app = require('../../app')
const {Car} = require('../../app/models')
jest.setTimeout(100000)


describe('create test (/v1/cars)', () => {
    let token;
    let tokenCustomer;

    beforeEach(async () => {
       token=await request(app)
            .post("/v1/auth/login")
            .send({
                email:"handayani@binar.co.id",
                password:"123456"
            })
            .then(res => {
                return res.body.accessToken
            })
    }) 
    beforeEach(async () => {
        tokenCustomer=await request(app)
             .post("/v1/auth/login")
             .send({
                 email:"brian@binar.co.id",
                 password:"123456"
             })
             .then(res => {
                 return res.body.accessToken
             })
     }) 
     
    it('jika status respon code 201',async () => {
        let data = {
            name:"avanza",
            price:1000,
            size:"SMALL",
            image:"avanza.jpg",
            isCurrentlyRented:false,
            createdAt:new Date(),
            updatedAt : new Date()
        }

        return request(app)
            .post("/v1/cars")
            .set("Authorization",`Bearer ${await token}`)
            .send(data)
            .then(res => {
                expect(res.status).toBe(201)
                expect(res.body).toEqual(res.body)
            })
    });
    it('jika status pesannya access forbidden',async () => {
        let data = {
            name:"avanza",
            price:1000,
            size:"SMALL",
            image:"avanza.jpg",
            isCurrentlyRented:false,
            createdAt:new Date(),
            updatedAt : new Date()
        }

        return request(app)
            .post("/v1/cars")
            .set("Authorization",`Bearer ${await tokenCustomer}`)
            .send(data)
            .then(res => {
                expect(res.body.error.message).toBe("Access forbidden!")
            })
    });
});