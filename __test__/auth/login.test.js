require('dotenv').config()
const res = require('express/lib/response');
const request = require('supertest')
const app = require('../../app')
jest.setTimeout(100000)


describe('login test (/v1/auth/login)', () => {
    it('jika status respon code 201', () => {
        return request(app)
            .post('/v1/auth/login')
            .send({
                email:"brian@binar.co.id",
                password:"123456"  
            })
            .then(res => {
                expect(res.status).toBe(201)
            })
    });
    it('jika status respon code 404', () => {
        return request(app)
            .post('/v1/auth/login')
            .send({
                email:"badil@binar.co.id",
                password:"123456"  
            })
            .then(res => {
                expect(res.status).toBe(404)
            })
    });
    it('jika status respon code 401', () => {
        return request(app)
            .post('/v1/auth/login')
            .send({
                email:"brian@binar.co.id",
                password:"binar123"  
            })
            .then(res => {
                expect(res.status).toBe(401)
            })
    });
    
});


