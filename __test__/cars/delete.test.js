require('dotenv').config()
const request = require('supertest')
const app = require('../../app')
const {Car} = require('../../app/models')
jest.setTimeout(100000)


describe('delete test (/v1/cars/:id)', () => {
    let token;
    let data 

    beforeEach(async () => {
       token=await request(app)
            .post("/v1/auth/login")
            .send({
                email:"handayani@binar.co.id",
                password:"123456"
            })
            .then(res => {
                return res.body.accessToken
            })
    }) 

    beforeEach(async () => {
        data = await Car.create({
            name:"avanza",
            price:1000,
            size:"SMALL",
            image:"avanza.jpg",
            isCurrentlyRented:false,
            createdAt:new Date(),
            updatedAt : new Date()
        })
    }) 
    
    
    it('jika status respon code 204',async () => {
        return request(app)
            .delete("/v1/cars/"+data.id)
            .set("Authorization",`Bearer ${await token}`)
            .send(data)
            .then(res => {
                expect(res.status).toBe(204)
                expect(res.body).toEqual(res.body)
            })
    });
    
});