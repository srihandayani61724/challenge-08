'use strict';
const bcrypt = require("bcryptjs");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const password = "123456";
     const encryptedPassword = bcrypt.hashSync(password, 10);

      await queryInterface.bulkInsert('Users', [{
        name: "handayani",
        email: `handayani@binar.co.id`,
        encryptedPassword,
        roleId: 2, 
        createdAt: new Date() ,
        updatedAt: new Date(),
      }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *m 
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};