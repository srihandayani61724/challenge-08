require('dotenv').config()
const request = require('supertest')
const app = require('../../app')
const {Car} = require('../../app/models')
jest.setTimeout(100000)


describe('rent test (/v1/cars/:id/rent)', () => {
    let token;

    beforeEach(async () => {
       token=await request(app)
            .post("/v1/auth/login")
            .send({
                email:"brian@binar.co.id",
                password:"123456"
            })
            .then(res => {
                return res.body.accessToken
            })
    }) 
    
    it('jika status respon code 201',async () => {
        let data = {
            rentStartedAt: new Date()
        }

        return request(app)
            .post("/v1/cars/5/rent")
            .set("Authorization",`Bearer ${await token}`)
            .send(data)
            .then(res => {
                expect(res.status).toBe(201)
                expect(res.body).toEqual(res.body)
            })
    });

    it('jika status respon code 422',async () => {
        let data = {
            rentStartedAt: "2022-06-01"
        }

        return request(app)
            .post("/v1/cars/5/rent")
            .set("Authorization",`Bearer ${await token}`)
            .send(data)
            .then(res => {
                expect(res.status).toBe(422)
                expect(res.body).toEqual(res.body)
            })
    });
   
});