require('dotenv').config()
const request = require('supertest')
const app = require('../../app')
jest.setTimeout(100000)


describe('getAllcars test (/v1/cars)', () => {
    it('jika status respon code 200', () => {
        return request(app)
            .get("/v1/cars")
            .then(res => {
                expect(res.status).toBe(200)
            })
    });
    
});
