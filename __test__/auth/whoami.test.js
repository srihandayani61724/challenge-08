require('dotenv').config()
const request = require('supertest')
const app = require('../../app')
const {Car} = require('../../app/models')
jest.setTimeout(100000)


describe('whoami test (/v1/cars/:id)', () => {
    let token;

    beforeEach(async () => {
       token=await request(app)
            .post("/v1/auth/login")
            .send({
                email:"brian@binar.co.id",
                password:"123456"
            })
            .then(res => {
                return res.body.accessToken
            })
    }) 
    
    it('jika status respon code 200',async () => {
        return request(app)
            .get("/v1/auth/whoami")
            .set("Authorization",`Bearer ${await token}`)
            .then(res => {
                expect(res.status).toBe(200)
                expect(res.body).toEqual(res.body)
            })
    });
    
});